
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class NumberPuzzles2 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("");
        System.out.println("1) Find two digits <= 56 with sums of digits > 10");
        System.out.println("2) Find two digit number minus number reversed which equals sum of digits");
        System.out.println("3) Quit");
        System.out.println(">");
        int choice = keyboard.nextInt();

        if (choice == 1) {
            function1();
        } else if (choice == 2) {
            function2();
        }
    }

    public static void function1() {
        for (int i = 1; i < 6; i++) {
            for (int n = 0; n < 10; n++) {
                int c = n + i;

                if (c > 10) {
                    int d = i * 10 + n;
                    if (i < 5 || n < 7) {
                        System.out.println(d);
                    }
                }
            }
        }

    }
    
    public static void function2() {
        for (int i = 1; i < 10; i++) {
            for (int n = 0; n < 10; n++) {
                int c = i + n;
                int d = i * 10 + n;
                int e = n * 10 + i;
                int sub = e - d;
                
                if (c == sub) {
                    System.out.println(d);
                }
            }
        }
    }
}
