
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class LetterRevisited {
    
    public static void main (String [] args) {

    Scanner keyboard = new Scanner(System.in);
    PrintWriter fileOut;
    
    try {
            fileOut = new PrintWriter("letter.txt");
        } catch (IOException e) {
            System.out.println("Sorry, I cannot open this file 'letter.txt' for editing.");
            System.out.println("Maybe the file exists and is read-only?");
            fileOut = null;
            System.exit(1);
        }
    
        fileOut.println("+--------------------------------------------------");
        fileOut.println("|                                              ###|");
        fileOut.println("|                                              ###|");
        fileOut.println("|                                              ###|");
        fileOut.println("|                                                 |");
        fileOut.println("|                                                 |");
        fileOut.println("|                 Bill Gates                      |");
        fileOut.println("|                 1 Microsoft Way                 |");
        fileOut.println("|                 Redmond, WA 98104               |");
        fileOut.println("|                                                 |");
        fileOut.println("+-------------------------------------------------+");
    
        fileOut.close();
    }
}
