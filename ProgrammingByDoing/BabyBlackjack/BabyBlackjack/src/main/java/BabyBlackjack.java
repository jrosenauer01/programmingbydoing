
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class BabyBlackjack {

    public static void main(String[] args) {
        Random r = new Random();

        int p1 = 1 + r.nextInt(10);
        int p2 = 1 + r.nextInt(10);
        int d1 = 1 + r.nextInt(10);
        int d2 = 1 + r.nextInt(10);
        int pTotal = p1 + p2;
        int dTotal = d1 + d2;

        System.out.println("Baby Blackjack");
        System.out.println("");
        System.out.println("You drew " + p1 + " and " + p2);
        System.out.println("Your total is " + pTotal);
        System.out.println("");
        System.out.println("The dealer has " + d1 + " and " + d2);
        System.out.println("Dealer's total is " + dTotal);

        if (dTotal >= pTotal) {
            System.out.println("THE DEALER WINS! SORRY!");
        } else {
            System.out.println("YOU WIN! CONGRATS!");
        }
    }
}
