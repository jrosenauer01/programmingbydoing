/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class FindingPrimeNumbers {

    public static void main(String[] args) {
        System.out.println("2 > ");
        for (int i = 3; i < 21; i++) {
            System.out.println("\n" + i + " ");
            if (isPrime(i)) {
                System.out.println("<");
            }
        }

    public static boolean isPrime ( int n) {
        boolean is = true;
        for (int i = 2; i < n ; i++) {
            if (n % i == 0) {
                is = false;
            }
        }
        return is;
    }
}
