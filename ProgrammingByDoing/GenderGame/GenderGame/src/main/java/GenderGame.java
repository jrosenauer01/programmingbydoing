
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class GenderGame {

    public static void main(String[] args) {

        String gender = "";
        int age = 0;
        String married = "";
        String firstName = "";
        String lastName = "";

        Scanner keyboard = new Scanner(System.in);

        System.out.println("What is your gender (M or F)?");
        gender = keyboard.next();
        System.out.println("First Name: ");
        firstName = keyboard.next();
        System.out.println("Last Name: ");
        lastName = keyboard.next();
        System.out.println("Age: ");
        age = keyboard.nextInt();

        if (gender == "F" && age >= 20) {
            System.out.println("Are you married, " + firstName + "(y or n)?");
            married = keyboard.next();
            System.out.println();
            if (married == "y") {
                System.out.println("Then I shall call you Mrs. " + lastName);
            }
            if (married == "no") {
                System.out.println("Then I shall call you Ms. " + lastName);
            }
        }
        if (age < 20) {
            System.out.println("Then I shall call you " + firstName + lastName);
        }
        if (gender == "M" && age >= 20) {
            System.out.println("Then I shall call you Mr. " + lastName);
        }
    }
}
