
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class LittleQuiz {

    public static void main(String[] args) {

        int choice = 0;
        String ready;
        String choice2;
        int countCorrect = 0;

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Are you ready for a quiz?");
        ready = keyboard.next();
        System.out.println("Okay, here it comes!");

        System.out.println("Q1) What is the capital of Alaska?"
                + "\n 1) Melbourne"
                + "\n 2) Anchorage"
                + "\n 3) Juneau");

        System.out.println("Select the number for your answer.");
        choice = keyboard.nextInt();

        if (choice == 3) {
            System.out.println("That is correct!");
            countCorrect++;
        } else if (choice == 2 || choice == 1) {
            System.out.println("That is incorrect.");
        } else {
            System.out.println("Please choose from the following answers.");
        }

        System.out.println("Q2) Can you store the value 'cat' in a variable of type int?"
                + "\n 1) Yes"
                + "\n 2) No");

        System.out.println("Select the number for your answer.");
        choice = keyboard.nextInt();

        if (choice == 2) {
            System.out.println("That's correct!");
            countCorrect++;
        } else {
            System.out.println("Sorry, 'cat' is a string, ints can only store numbers.");
        }

        System.out.println("Q1) What is the result of 9+6/3?"
                + "\n 1) 5"
                + "\n 2) 11"
                + "\n 3) 2");

        System.out.println("Select the number for your answer.");
        choice = keyboard.nextInt();

        if (choice == 2) {
            System.out.println("That is correct!");
            countCorrect++;
        } else if (choice == 1 || choice == 3) {
            System.out.println("That is incorrect.");
        } else {
            System.out.println("Please choose from the following answers.");
        }

        System.out.println("Overall, you got " + countCorrect + " out of 3 correct.");
        System.out.println("Thanks for playing!");
    }
}
