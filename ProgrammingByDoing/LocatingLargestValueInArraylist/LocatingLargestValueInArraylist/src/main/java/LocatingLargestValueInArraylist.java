
import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class LocatingLargestValueInArraylist {
        public static void main(String[] args) {

        ArrayList<Integer> arrlist = new ArrayList<Integer>();

        arrlist.add(10);
        arrlist.add(23);
        arrlist.add(43);
        arrlist.add(38);
        arrlist.add(26);
        arrlist.add(7);
        arrlist.add(15);
        arrlist.add(49);
        arrlist.add(0);
        arrlist.add(22);
        arrlist.add(11);

Object obj = Collections.max(arrlist);
    System.out.println("The largest value is " + obj + ", which is in slot " + arrlist.indexOf(obj));
    }
}
