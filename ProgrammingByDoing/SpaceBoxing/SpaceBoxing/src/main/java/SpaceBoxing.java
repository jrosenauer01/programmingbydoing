
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SpaceBoxing {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        double weight;
        int planetChoice;

        System.out.println("Please enter your current Earth weight: ");
        weight = keyboard.nextInt();

        System.out.println("I have information for the following planets:"
                + "\n1. Venus  2. Mars  3. Juniper"
                + "\n4. Saturn  5. Uranus  6. Neptune");

        System.out.println("Which planet are you visiting?");
        planetChoice = keyboard.nextInt();

        if (planetChoice == 1) {
            weight = weight * 0.78;
            System.out.println("Your weight would be " + weight + " pounds on that planet.");
        } else if (planetChoice == 2) {
            weight = weight * 0.39;
            System.out.println("Your weight would be " + weight + " pounds on that planet.");
        } else if (planetChoice == 3) {
            weight = weight * 2.65;
            System.out.println("Your weight would be " + weight + " pounds on that planet.");
        } else if (planetChoice == 4) {
            weight = weight * 1.17;
            System.out.println("Your weight would be " + weight + " pounds on that planet.");
        } else if (planetChoice == 5) {
            weight = weight * 1.05;
            System.out.println("Your weight would be " + weight + " pounds on that planet.");
        } else if (planetChoice == 6) {
            weight = weight * 1.23;
            System.out.println("Your weight would be " + weight + " pounds on that planet.");
        } else {
            System.out.println("Please choose a given planet.");
        }
    }
}
