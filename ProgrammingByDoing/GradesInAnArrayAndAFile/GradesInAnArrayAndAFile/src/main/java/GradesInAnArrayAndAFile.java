
import java.io.*;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class GradesInAnArrayAndAFile {

    public static void main(String[] args) {

        PrintWriter fileOut;
        Scanner keyboard = new Scanner(System.in);
        String firstname ="";
        String lastname = "";
        String filename = "";
        //int[] randomNumbers = new int[5];

        System.out.println("Name (first):");
        firstname = keyboard.next();
        System.out.println("Name (last):");
        lastname = keyboard.next();
        System.out.println("File Name: ");
        filename = keyboard.next();

        System.out.println("Here are your randomly selected grades (hope you pass):");

        try {
            FileWriter fw = new FileWriter(filename, false);
            BufferedWriter bw = new BufferedWriter(fw);
            
            System.out.println(firstname + " " + lastname);
            bw.write(firstname + " " + lastname);
            bw.newLine();

            Random r = new Random();
            for (int i = 1; i < 6; i++) {
                int n = r.nextInt(100);

                System.out.println("Grade " + i + ": " + n);
                bw.write("Grade " + i + ": " + n);
                bw.newLine();
            }
            bw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }
}
