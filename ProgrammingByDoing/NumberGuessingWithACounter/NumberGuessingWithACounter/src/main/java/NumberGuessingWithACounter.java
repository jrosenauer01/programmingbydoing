
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class NumberGuessingWithACounter {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        Random r = new Random();

        int guess = 0;
        int number = 1 + r.nextInt(10);
        int tries = 1;

        System.out.println("I have chosen a number between 1 and 10. Try to guess it.");
        System.out.println("Your guess: ");
        guess = keyboard.nextInt();

        while (guess != number) {
            System.out.println("That is incorrect. Guess again");
            guess = keyboard.nextInt();
            tries++;
        }
        
        System.out.println("That's right! You're a good guesser!");
        System.out.println("It only took you " + tries + " tries.");
    }

}
