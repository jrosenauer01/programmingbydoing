
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AgeMessages3 {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int age = 0;
        String name;

        System.out.println("Your name: ");
        name = keyboard.next();

        System.out.println("Your age: ");
        age = keyboard.nextInt();

        if (age < 16) {
            System.out.println("You can't drive.");
        } else if (age == 16 && age == 17) {
            System.out.println("You can drive but not vote.");
        } else if (age >= 18 && age <= 24) {
            System.out.println("You can vote but not rent a car.");
        } else {
            System.out.println("You can do pretty much anything.");
        }

    }

}
