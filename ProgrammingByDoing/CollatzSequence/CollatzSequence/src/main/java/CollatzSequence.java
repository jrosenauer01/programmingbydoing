
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CollatzSequence {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int n;

        System.out.println("Starting number: ");
        n = keyboard.nextInt();

        while (n != 1) {
            if (n % 2 == 0) {
                n = n / 2;
            } else if (n % 2 != 0) {
                n = (3 * n) + 1;
            }
            System.out.println(n);
        }
    }
}
