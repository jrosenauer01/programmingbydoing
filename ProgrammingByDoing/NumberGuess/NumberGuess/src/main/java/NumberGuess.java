
import static java.lang.Math.random;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class NumberGuess {

    public static void main(String[] args) {
        Random r = new Random();

        Scanner keyboard = new Scanner(System.in);

        int guess = 0;
        int n = 1 + r.nextInt(10);

        System.out.println("I am thinking of a number between 1 and 10.");
        System.out.println("Your guess: ");
        guess = keyboard.nextInt();

        if (guess == n) {
            System.out.println("That is correct.");
        } else {
            System.out.println("Sorry that is the wrong number.");
        }
    }
}
