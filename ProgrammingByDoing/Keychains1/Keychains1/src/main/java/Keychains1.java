
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Keychains1 {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int choice = 1;

        System.out.println("Ye Olde Keychain Shoppe");
        System.out.println("");

        while (choice != 4) {
            System.out.println("1. Add Keychains to Order");
            System.out.println("2. Remove Keychains from Order");
            System.out.println("3. View Current Order");
            System.out.println("4. Checkout");
            System.out.println();
            System.out.println("Please enter your choice: ");
            choice = keyboard.nextInt();
            System.out.println();

            if (choice == 1) {
                addKeychains();
            } else if (choice == 2) {
                removeKeychains();
            } else if (choice == 3) {
                viewOrder();
            } else if (choice == 4) {
                Checkout();
            } else {
                System.out.println("Please choose one of the followin options.");
            }

            System.out.println();
        }
    }

    public static void addKeychains() {
        System.out.println("ADD KEYCHAINS");
    }
    public static void removeKeychains() {
        System.out.println("REMOVE KEYCHAINS");
    }
    public static void viewOrder() {
        System.out.println("VIEW ORDER");
    }
    public static void Checkout() {
        System.out.println("CHECKOUT");
    }
}
