
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Nim {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        Random r = new Random();

        String player1 = "";
        String player2 = "";
        int a = 1 + r.nextInt(5);
        int b = 1 + r.nextInt(5);
        int c = 1 + r.nextInt(5);
        String choice;
        int many;

        System.out.println("Player 1, please enter your name: ");
        player1 = keyboard.next();
        System.out.println("Player 2, please enter your name: ");
        player2 = keyboard.next();
        System.out.println("");

        while (a != 0 || b != 0 || c != 0) {
            System.out.println("A: " + a + "B: " + b + "C: " + c);

            System.out.println(player1 + ", choose a pile: ");
            choice = keyboard.next();
            System.out.println("How many to remove from pile " + choice);
            many = keyboard.nextInt();
            System.out.println("");

            if (choice.equals("a") || choice.equals("A")) {
                while (many > a || many < 1) {
                    System.out.println("");
                }

                a -= many;
            } else if (choice.equals("b") || choice.equals("B")) {
                while (many > b || many < 1) {
                    System.out.println("");
                }

                b -= many;
            } else if (choice.equals("c") || choice.equals("C")) {
                while (many > c || many < 1) {
                    System.out.println("");
                }

                c -= many;
            } 
        }
    }
}
