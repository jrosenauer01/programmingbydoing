
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class HowOldAreYou {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int age;
        String name;

        System.out.println("Hey, what's your name?");
        name = keyboard.next();
        System.out.println("Ok, " + name + ", how old are you?");
        age = keyboard.nextInt();

        if (age < 16) {
            System.out.println("You cannot drive.");
        }
        if (age < 18) {
            System.out.println("You cannot vote.");
        }
        if (age < 25) {
            System.out.println("You cannot rent a car.");
        }
        if (age >= 25) {
            System.out.println("You can do anything that is legal.");
        }
    }
}
