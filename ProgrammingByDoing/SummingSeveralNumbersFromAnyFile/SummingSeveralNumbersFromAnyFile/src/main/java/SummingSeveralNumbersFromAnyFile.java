
import java.io.File;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class SummingSeveralNumbersFromAnyFile {
    
    public static void main (String [] args) throws Exception {
        
        Scanner keyboard = new Scanner (System.in);
        
        int sum = 0;
        
        System.out.println("Which file would you like to read from: ");
        String file = keyboard.next();
        System.out.println("Reading numbers from \"" + file + "\"\n");
        System.out.println();
        
        Scanner reader = new Scanner ( new File(file));
        
        while (reader.hasNext()) {
            int n = reader.nextInt();
            sum += n;
            System.out.println(n + " ");
        }
        System.out.println("\nTotal is " + sum);
    }
}
