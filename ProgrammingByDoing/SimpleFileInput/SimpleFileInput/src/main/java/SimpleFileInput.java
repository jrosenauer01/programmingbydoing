
import java.io.File;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class SimpleFileInput {
    
    public static void main (String [] args) throws Exception {
        
        String name;
        
        Scanner keyboard = new Scanner (new File("name.txt"));
        name = keyboard.nextLine();
        
        keyboard.close();
                
        System.out.println("Your name is: " + name);
    }
}
