
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SquareRoot {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int n;

        System.out.println("SQUARE ROOT!");
        System.out.println("Enter a number: ");
        n = keyboard.nextInt();
        double sr = Math.sqrt(n);

        while (n <= 0) {

            System.out.println("You cannot take the square root of a negative number. Please try again.");
            n = keyboard.nextInt();
            sr = Math.sqrt(n);
        }

        System.out.println("The square root of " + n + " is " + sr);

    }
}
