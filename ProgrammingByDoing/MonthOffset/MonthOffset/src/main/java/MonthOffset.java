/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class MonthOffset {

    public static int monthOffset(int month) {
        int result = 1;
        
        if (month == 1 || month == 10) {
            result = 1;
        } else if (month == 5) {
            result = 2;
        } else if (month == 8) {
            result = 3;
        } else if (month == 2 || month == 3 || month == 11) {
            result = 4;
        } else if (month == 9 || month == 12) {
            result = 6;
        } else if (month == 4 || month == 7) {
            result = 0;
        } else if (month == 6) {
            result = 5;
        }
        return result;
    }

    public static void main(String[] args) {
        
        System.out.println("Offset for month 1: " + monthOffset(1));
        System.out.println("Offset for month 2: " + monthOffset(2));
        System.out.println("Offset for month 3: " + monthOffset(3));
        System.out.println("Offset for month 4: " + monthOffset(4));
        System.out.println("Offset for month 5: " + monthOffset(5));
        System.out.println("Offset for month 6: " + monthOffset(6));
        System.out.println("Offset for month 7: " + monthOffset(7));
        System.out.println("Offset for month 8: " + monthOffset(8));
        System.out.println("Offset for month 9: " + monthOffset(9));
        System.out.println("Offset for month 10: " + monthOffset(10));
        System.out.println("Offset for month 11: " + monthOffset(11));
        System.out.println("Offset for month 12: " + monthOffset(12));
        System.out.println("Offset for month 43: " + monthOffset(43));
    }
}
