
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CompareToChallenge {

    public static void main(String[] args) {

        //Less than 0
        System.out.println("Comparing \"bape\" with \"cape\" produces ");
        System.out.println("bape".compareTo("cape"));

        System.out.println("Comparing \"sape\" with \"tape\" produces ");
        System.out.println("sape".compareTo("tape"));

        System.out.println("Comparing \"bat\" with \"cat\" produces ");
        System.out.println("bat".compareTo("cat"));

        System.out.println("Comparing \"mat\" with \"nat\" produces ");
        System.out.println("mat".compareTo("nat"));

        System.out.println("Comparing \"sail\" with \"tail\" produces ");
        System.out.println("sail".compareTo("tail"));

        //Greater than 1
        System.out.println("Comparing \"tablet\" with \"apple\" produces ");
        System.out.println("tablet".compareTo("apple"));

        System.out.println("Comparing \"ostrich\" with \"berry\" produces ");
        System.out.println("ostrich".compareTo("berry"));

        System.out.println("Comparing \"dogs\" with \"dog\" produces ");
        System.out.println("lemon".compareTo("black"));

        System.out.println("Comparing \"dogs\" with \"dog\" produces ");
        System.out.println("thing".compareTo("anything"));

        System.out.println("Comparing \"dogs\" with \"dog\" produces ");
        System.out.println("much".compareTo("alot"));

        //Equal to 0
        System.out.println("Comparing \"bank\" with \"bank\" produces ");
        System.out.println("bank".compareTo("bank"));

        System.out.println("Comparing \"angle\" with \"angle\" produces ");
        System.out.println("angle".compareTo("angle"));
    }
}
