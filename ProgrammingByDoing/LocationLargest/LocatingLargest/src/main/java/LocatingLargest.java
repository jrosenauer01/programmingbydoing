
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class LocatingLargest {

    public static void main(String[] args) {

        int[] arrlist = {3, 55, 42, 66, 48, 79, 97, 32, 83, 99};

        for (int i = 0; i < arrlist.length; i++) {
            System.out.println(arrlist[i] + " ");
        }
        // Finding the largest element
        double max = arrlist[0];
        for (int i = 1; i < arrlist.length; i++) {
            if (arrlist[i] > max) {
                max = arrlist[i];
            }
        }
        System.out.println("The largest value is " + max);
        
        Arrays.asList(arrlist).indexOf(max);
        System.out.println("It is in slot " + (Arrays.asList(arrlist).indexOf(max)));
    }

}
