
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CountingWhile {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Type in a message, and I'll display it five times.");
        System.out.print("Message: "); String message = keyboard.nextLine();
        System.out.println("How many times?"); int times = keyboard.nextInt();

        int n = 0;
        while (n < 30) {
            System.out.println((n + 10) + ". " + message);
            n+=10;
        }

    }
}
