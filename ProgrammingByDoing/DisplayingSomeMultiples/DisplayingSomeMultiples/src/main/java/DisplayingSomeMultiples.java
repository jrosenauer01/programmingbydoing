
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class DisplayingSomeMultiples {
    public static void main (String [] args) {
        Scanner keyboard = new  Scanner (System.in);
        
        System.out.println("Choose a number: ");
        int number = keyboard.nextInt();
        
        for (int i = 1; i < 13; i++) {
            int mult = i * number;
            System.out.println(number + "x" + i + " = " + mult);
        }
    }
}
