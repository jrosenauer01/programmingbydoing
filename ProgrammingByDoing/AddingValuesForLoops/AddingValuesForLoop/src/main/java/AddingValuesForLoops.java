
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AddingValuesForLoops {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        int number;
        int sum = 0;

        System.out.println("Number: ");
        number = keyboard.nextInt();

        for (int n = 1; n <= number; n += 1) {
            System.out.println(n + " ");

            sum += n;
        }
        
        System.out.println("");
        System.out.println("The sum is " + sum);
        System.out.println("");
    }
}
