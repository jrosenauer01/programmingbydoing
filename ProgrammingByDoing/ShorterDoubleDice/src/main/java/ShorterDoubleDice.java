
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class ShorterDoubleDice {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        Random r = new Random();
        
        int dice1, dice2, total;
        
        System.out.println("HERE COMES THE DICE!");
        System.out.println("");

        do {
            dice1 = 1 + r.nextInt(6);
            dice2 = 2 + r.nextInt(6);
            int sum = dice1 + dice2;

            System.out.println("Roll #1: " + dice1);
            System.out.println("Roll #2: " + dice2);
            System.out.println("The total is " + sum);
            
        } while (dice1 != dice2);
    }
}
